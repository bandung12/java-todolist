CREATE TABLE todo
(
    id        BIGSERIAL NOT NULL,
    title     VARCHAR(255),
    completed BOOLEAN,
    CONSTRAINT pk_todo PRIMARY KEY (id)
);